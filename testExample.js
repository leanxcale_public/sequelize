const Sequelize = require("./index");
const { DataTypes } = require("./index");

try {
  const seq = new Sequelize("lxdb://7c65a5a79583c4b77bbe.lxc-db.com:1522/db", {
    username: "user5",
    password: "user5",
  });
  class Person extends Sequelize.Model {}

  Person.init(
    {
      // Model attributes are defined here
      ID: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        // allowNull defaults to true
      },
      NAME: {
        type: DataTypes.STRING,
        // allowNull defaults to true
      },
      LASTNAME: {
        type: DataTypes.STRING,
        // allowNull defaults to true
      },
      PHONE: {
        type: DataTypes.STRING,
      },
    },
    {
      timestamps: false,
      tableName: "Person",
      // Other model options go here
      sequelize: seq, // We need to pass the connection instance,
      modelName: "Person", // We need to choose the model name
    }
  );
  seq.sync({ force: true }).then(() => {
    Person.findAndCountAll({
      where: {
        NAME: {
          [Sequelize.Op.ne]: "Toptal",
        },
        LASTNAME: "Interview",
        PHONE: "21421421",
      },
    }).then((data) => {
      console.log(data);
    });
  });
} catch (err) {
  console.log(err);
}
