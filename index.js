const sequelize = require("sequelize");
const sequelizeOriginal = require("sequelize-original");
const url = require("url");

const MysqlDialect = require("sequelize/lib/dialects/mysql/index");
const MySqlConnectionManager = require("sequelize/lib/dialects/mysql/connection-manager");
const MySqlQueryGenerator = require("sequelize/lib/dialects/mysql/query-generator");
const {
  MySQLQueryInterface,
} = require("sequelize/lib/dialects/mysql/query-interface");

const DataTypes = require("./DataTypes");

const QueryTypes = require("sequelize/lib/query-types");

const LxDialect = require("./lxdb");
const LxQueryGenerator = require("./lxdb/query-generator");
const { LxDBQueryInterface } = require("./lxdb/query-interface");
const LxConnectionManager = require("./lxdb/connection-manager");

Object.getOwnPropertyNames(MysqlDialect.prototype).forEach((prototypeName) => {
  if (!Object.getOwnPropertyNames(LxDialect.prototype).includes(prototypeName))
    delete MysqlDialect.prototype[prototypeName];
});

Object.getOwnPropertyNames(MySqlQueryGenerator.prototype).forEach(
  (prototypeName) => {
    if (
      !Object.getOwnPropertyNames(LxQueryGenerator.prototype).includes(
        prototypeName
      )
    )
      delete MySqlQueryGenerator.prototype[prototypeName];
  }
);

Object.getOwnPropertyNames(MySqlConnectionManager.prototype).forEach(
  (prototypeName) => {
    if (
      !Object.getOwnPropertyNames(LxConnectionManager.prototype).includes(
        prototypeName
      )
    )
      delete MySqlConnectionManager.prototype[prototypeName];
  }
);

Object.getOwnPropertyNames(MySQLQueryInterface.prototype).forEach(
  (prototypeName) => {
    if (
      !Object.getOwnPropertyNames(LxDBQueryInterface.prototype).includes(
        prototypeName
      )
    )
      delete MySQLQueryInterface.prototype[prototypeName];
  }
);

Object.getOwnPropertyNames(LxDialect.prototype).forEach((prototypeName) => {
  MysqlDialect.prototype[prototypeName] = LxDialect.prototype[prototypeName];
});

Object.getOwnPropertyNames(LxQueryGenerator.prototype).forEach(
  (prototypeName) => {
    MySqlQueryGenerator.prototype[prototypeName] =
      LxQueryGenerator.prototype[prototypeName];
  }
);

Object.getOwnPropertyNames(LxConnectionManager.prototype).forEach(
  (prototypeName) => {
    MySqlConnectionManager.prototype[prototypeName] =
      LxConnectionManager.prototype[prototypeName];
  }
);

Object.getOwnPropertyNames(LxDBQueryInterface.prototype).forEach(
  (prototypeName) => {
    MySQLQueryInterface.prototype[prototypeName] =
      LxDBQueryInterface.prototype[prototypeName];
  }
);

const BASE_DIALECT = "mysql";
const LX_DIALECT = "lxdb";

const getDialectFromArgs = (...args) => {
  const [database, username, password, options] = args;
  let dialect;
  let dialectContainer;
  if (args.length === 1 && typeof database === "object") {
    dialect = database.dialect;
    dialectContainer = "database-object";
  } else if (
    (args.length === 1 && typeof database === "string") ||
    (args.length === 2 && typeof username === "object")
  ) {
    const urlParts = url.parse(args[0], true);
    dialect = urlParts.protocol.replace(/:$/, "");
    dialectContainer = "database-url";
  } else {
    dialect = options?.dialect;
    dialectContainer = "options";
  }
  return {
    dialect,
    dialectContainer,
  };
};

const normalizeDialectForLX = (args) => {
  let { dialect, dialectContainer } = getDialectFromArgs(...args);

  // Change LX_DIALECT to BASE_DIALECT(mysql) as Sequelize Base Constructor could not read LX_DIALECT.
  if (dialect === LX_DIALECT) {
    if (dialectContainer === "database-object") {
      args[0].dialect = BASE_DIALECT;
    } else if (dialectContainer === "database-url") {
      args[0] = args[0].replace(LX_DIALECT, BASE_DIALECT);
    }
    if (dialectContainer === "options" && args[3]) {
      args[3].dialect = BASE_DIALECT;
    }
  }
  return dialect;
};

class SequelizeLx extends sequelize.Sequelize {
  constructor(...args) {
    if (args[args.length - 1] !== "DIALECT_CONFIGURED")
      normalizeDialectForLX(args);
    else args.splice(-1);
    super(...args);
  }
  async authenticate(options) {
    options = {
      raw: true,
      plain: true,
      type: QueryTypes.SELECT,
      ...options,
    };
    await this.query('SELECT 1+1 AS "result"', options);
  }
}

class Sequelize {
  constructor(...args) {
    let dialect = normalizeDialectForLX(args);
    try {
      if (dialect === "lxdb") {
        args.push("DIALECT_CONFIGURED");
        return new SequelizeLx(...args);
      } else {
        return new sequelizeOriginal.Sequelize(...args);
      }
    } catch (err) {
      if (
        err.message ===
        `The dialect ${dialect} is not supported. Supported dialects: mssql, mariadb, mysql, postgres, db2 and sqlite.`
      )
        throw new Error(
          `The dialect ${dialect} is not supported. Supported dialects: mssql, mariadb, mysql, postgres, db2, sqlite and lxdb.`
        );
      else throw err;
    }
  }
}

Object.getOwnPropertyNames(sequelize.prototype).forEach((prototypeName) => {
  Sequelize.prototype[prototypeName] = sequelize.prototype[prototypeName];
});

Object.keys(sequelize).map((key) => {
  Sequelize[key] = sequelize[key];
});
Sequelize.DataTypes = DataTypes;

module.exports = Sequelize;
module.exports.Sequelize = Sequelize;
module.exports.default = Sequelize;
module.exports.TestSequelize = SequelizeLx;
