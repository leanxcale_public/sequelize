const DataTypes = require("sequelize/lib/data-types");

const lxdbDialectTypes = require("./lxdb/data-types")(DataTypes);
Object.keys(lxdbDialectTypes).forEach((type) => {
  Object.getOwnPropertyNames(lxdbDialectTypes[type].prototype).forEach(
    (prototypeName) => {
      if (DataTypes.mysql[type])
        DataTypes.mysql[type].prototype[prototypeName] =
          lxdbDialectTypes[type].prototype[prototypeName];
      else {
        DataTypes[type].prototype[prototypeName] =
          lxdbDialectTypes[type].prototype[prototypeName];
      }
    }
  );
});

module.exports = DataTypes;
