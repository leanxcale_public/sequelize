"use strict";

const _ = require("lodash");
const Utils = require("sequelize/lib/utils");
const AbstractQueryGenerator = require("sequelize/lib/dialects/abstract/query-generator");
const util = require("util");
const Op = require("sequelize/lib/operators");

const { logger } = require("sequelize/lib/utils/logger");
const debuglxdb = logger.debugContext("querygen:lxdb");
const QueryTypes = require("sequelize/lib/query-types");

require("json-circular-stringify"); //lxdb

const JSON_FUNCTION_REGEX =
  /^\s*((?:[a-z]+_){0,2}jsonb?(?:_[a-z]+){0,2})\([^)]*\)/i;
const JSON_OPERATOR_REGEX = /^\s*(->>?|@>|<@|\?[|&]?|\|{2}|#-)/i;
const TOKEN_CAPTURE_REGEX =
  /^\s*((?:([""'])(?:(?!\2).|\2{2})*\2)|[\w\d\s]+|[().,;+-])/i;
const FOREIGN_KEY_FIELDS = [
  "FKNAME as constraint_name",
  "FKNAME as constraintName",
  "FKTABLESCHEM as constraintSchema",
  "FKTABLESCHEM as constraintCatalog",
  "FKTABLENAME as tableName",
  "FKTABLESCHEM as tableSchema",
  "FKTABLESCHEM as tableCatalog",
  "FKCOLUMNNAME as columnName",
  "PKTABLESCHEM as referencedTableSchema",
  "PKTABLESCHEM as referencedTableCatalog",
  "PKTABLENAME as referencedTableName",
  "PKCOLUMNNAME as referencedColumnName",
].join(",");

const typeWithoutDefault = new Set(["BLOB", "TEXT", "GEOMETRY", "JSON"]);

class LxDBQueryGenerator extends AbstractQueryGenerator {
  constructor(options) {
    super(options);
    this.OperatorMap = {
      ...this.OperatorMap,
      [Op.ne]: "<>",
      [Op.regexp]: "SIMILAR TO",
      [Op.notRegexp]: "NOT SIMILAR TO",
      [Op.iRegexp]: "SIMILAR TO",
      [Op.notIRegexp]: "NOT SIMILAR TO",
    };
  }

  quoteIdentifier(identifier, force) {
    this.OperatorMap = {
      ...this.OperatorMap,
      [Op.ne]: "<>",
      [Op.regexp]: "SIMILAR TO",
      [Op.notRegexp]: "NOT SIMILAR TO",
      [Op.iRegexp]: "SIMILAR TO",
      [Op.notIRegexp]: "NOT SIMILAR TO",
    };
    const reservedWords =
      "all,analyse,analyze,and,any,array,as,asc,asymmetric,authorization,binary,both,case,cast,check,collate,collation,column,concurrently,constraint,create,cross,current_catalog,current_date,current_role,current_schema,current_time,current_timestamp,current_user,default,deferrable,desc,distinct,do,else,end,except,false,fetch,for,foreign,freeze,from,full,grant,group,having,ilike,in,initially,inner,intersect,into,is,isnull,join,lateral,leading,left,like,limit,localtime,localtimestamp,natural,not,notnull,null,offset,on,only,or,order,outer,overlaps,placing,primary,references,returning,right,select,session_user,similar,some,symmetric,table,tablesample,then,to,trailing,true,union,unique,user,using,variadic,verbose,when,where,window,with".split(
        ","
      );
    let options = {
      force,
      quoteIdentifiers: this.options.quoteIdentifiers,
    };
    if (identifier === "*") return identifier;
    options = Utils.defaults(options || {}, {
      force: false,
      quoteIdentifiers: true,
    });
    const rawIdentifier = Utils.removeTicks(identifier, '"');
    if (
      options.force !== true &&
      options.quoteIdentifiers === false &&
      !identifier.includes(".") &&
      !identifier.includes("->") &&
      !reservedWords.includes(rawIdentifier.toLowerCase())
    ) {
      return rawIdentifier;
    }
    return Utils.addTicks(rawIdentifier, '"');
  }

  createDatabaseQuery(databaseName, options) {
    throw new Error(
      "Database should be created in advance. This Lx driver is still not fully supporting DDL"
    );
  }

  dropDatabaseQuery(databaseName) {
    throw new Error(
      "DROP Database not implemented. This Lx driver is still not fully supporting DDL"
    );
  }

  createSchema() {
    throw new Error(
      "CREATE Schema not implemented. This Lx driver is still not fully supporting DDL"
    );
  }

  jsonPathExtractionQuery(column, path, isJson) {
    let paths = _.toPath(path);
    const quotedColumn = this.isIdentifierQuoted(column)
      ? column
      : this.quoteIdentifier(column);
    let pathStr = this.escape(
      ["$"]
        .concat(paths)
        .join(".")
        .replace(/\.(\d+)(?:(?=\.)|$)/g, (__, digit) => `[${digit}]`)
    );

    return `json_value(${quotedColumn},${pathStr})`;
  }

  generateReturnValues(modelAttributes, options) {
    const returnFields = [];
    let outputFragment = "";
    let returningFragment =
      options.type !== QueryTypes.UPSERT ? " RETURNINGSEQ" : "";
    let tmpTable = "";
    return { outputFragment, returnFields, returningFragment, tmpTable };
  }

  generateThroughJoin(include, includeAs, parentTableName, topLevelInfo) {
    const through = include.through;
    const throughTable = through.model.getTableName();
    const throughAs = `${includeAs.internalAs}->${through.as}`;
    const externalThroughAs = `${includeAs.externalAs}.${through.as}`;
    const throughAttributes = through.attributes.map((attr) => {
      let alias = `${externalThroughAs}.${
        Array.isArray(attr) ? attr[1] : attr
      }`;

      if (this.options.minifyAliases) {
        alias = this._getMinifiedAlias(alias, throughAs, topLevelInfo.options);
      }

      return Utils.joinSQLFragments([
        `${this.quoteIdentifier(throughAs)}.${this.quoteIdentifier(
          Array.isArray(attr) ? attr[0] : attr
        )}`,
        "AS",
        this.quoteIdentifier(alias),
      ]);
    });
    const association = include.association;
    const parentIsTop =
      !include.parent.association &&
      include.parent.model.name === topLevelInfo.options.model.name;
    const tableSource = parentTableName;
    const identSource = association.identifierField;
    const tableTarget = includeAs.internalAs;
    const identTarget = association.foreignIdentifierField;
    const attrTarget = association.targetKeyField;

    const joinType = include.required
      ? "INNER JOIN"
      : include.right && this._dialect.supports["RIGHT JOIN"]
      ? "RIGHT OUTER JOIN"
      : "LEFT OUTER JOIN";
    let joinBody;
    let joinCondition;
    const attributes = {
      main: [],
      subQuery: [],
    };
    let attrSource = association.sourceKey;
    let sourceJoinOn;
    let targetJoinOn;
    let throughWhere;
    let targetWhere;

    if (topLevelInfo.options.includeIgnoreAttributes !== false) {
      // Through includes are always hasMany, so we need to add the attributes to the mainAttributes no matter what (Real join will never be executed in subquery)
      for (const attr of throughAttributes) {
        attributes.main.push(attr);
      }
    }

    // Figure out if we need to use field or attribute
    if (!topLevelInfo.subQuery) {
      attrSource = association.sourceKeyField;
    }
    if (
      topLevelInfo.subQuery &&
      !include.subQuery &&
      !include.parent.subQuery &&
      include.parent.model !== topLevelInfo.options.mainModel
    ) {
      attrSource = association.sourceKeyField;
    }

    // Filter statement for left side of through
    // Used by both join and subquery where
    // If parent include was in a subquery need to join on the aliased attribute
    if (
      topLevelInfo.subQuery &&
      !include.subQuery &&
      include.parent.subQuery &&
      !parentIsTop
    ) {
      // If we are minifying aliases and our JOIN target has been minified, we need to use the alias instead of the original column name
      const joinSource =
        this._getAliasForField(
          tableSource,
          `${tableSource}.${attrSource}`,
          topLevelInfo.options
        ) || `${tableSource}.${attrSource}`;

      sourceJoinOn = `${this.quoteIdentifier(joinSource)} = `;
    } else {
      // If we are minifying aliases and our JOIN target has been minified, we need to use the alias instead of the original column name
      const aliasedSource =
        this._getAliasForField(tableSource, attrSource, topLevelInfo.options) ||
        attrSource;

      sourceJoinOn = `${this.quoteTable(tableSource)}.${this.quoteIdentifier(
        aliasedSource
      )} = `;
    }
    sourceJoinOn += `${this.quoteIdentifier(throughAs)}.${this.quoteIdentifier(
      identSource
    )}`;

    // Filter statement for right side of through
    // Used by both join and subquery where
    targetJoinOn = `${this.quoteIdentifier(tableTarget)}.${this.quoteIdentifier(
      attrTarget
    )} = `;
    targetJoinOn += `${this.quoteIdentifier(throughAs)}.${this.quoteIdentifier(
      identTarget
    )}`;

    if (through.where) {
      throughWhere = this.getWhereConditions(
        through.where,
        this.sequelize.literal(this.quoteIdentifier(throughAs)),
        through.model
      );
    }

    // Generate a wrapped join so that the through table join can be dependent on the target join
    joinBody = `${this.quoteTable(
      throughTable,
      throughAs
    )} ON ${sourceJoinOn} ${
      throughWhere ? `AND ${throughWhere}` : ""
    } ${joinType} ${this.quoteTable(
      include.model.getTableName(),
      includeAs.internalAs
    )}`;
    joinCondition = targetJoinOn;

    if (include.where || include.through.where) {
      if (include.where) {
        targetWhere = this.getWhereConditions(
          include.where,
          this.sequelize.literal(this.quoteIdentifier(includeAs.internalAs)),
          include.model,
          topLevelInfo.options
        );
        if (targetWhere) {
          joinCondition += ` AND ${targetWhere}`;
        }
      }
    }

    this._generateSubQueryFilter(include, includeAs, topLevelInfo);

    return {
      join: joinType,
      body: joinBody,
      condition: joinCondition,
      attributes,
    };
  }

  _traverseJSON(items, baseKey, prop, item, path) {
    let cast;

    if (path[path.length - 1].includes("::")) {
      const tmp = path[path.length - 1].split("::");
      cast = tmp[1];
      path[path.length - 1] = tmp[0];
    }

    let pathKey = this.jsonPathExtractionQuery(baseKey, path);

    if (_.isPlainObject(item)) {
      Utils.getOperators(item).forEach((op) => {
        const value = this._toJSONValue(item[op]);
        let isJson = false;
        if (typeof value === "string" && op === Op.contains) {
          try {
            JSON.stringify(value);
            isJson = true;
          } catch (e) {
            // failed to parse, is not json so isJson remains false
          }
        }
        pathKey = this.jsonPathExtractionQuery(baseKey, path, isJson);
        items.push(
          this.whereItemQuery(this._castKey(pathKey, value, cast), {
            [op]: value,
          })
        );
      });
      _.forOwn(item, (value, itemProp) => {
        this._traverseJSON(
          items,
          baseKey,
          itemProp,
          value,
          path.concat([itemProp])
        );
      });

      return;
    }

    item = this._toJSONValue(item);
    items.push(
      this.whereItemQuery(this._castKey(pathKey, item, cast), {
        [item === null ? Op.is : Op.eq]: item,
      })
    );
  }

  showSchemasQuery() {
    return 'select distinct tableschem as "schema_name" from SYSMETA.TABLES';
  }

  versionQuery() {
    return "SELECT '1.7' as \"version\"";
  }

  createTableQuery(tableName, attributes, options) {
    options = {
      charset: null,
      rowFormat: null,
      ...options,
    };
    // console.log("createTableQuery " + tableName + " #1# " + JSON.stringify(attributes) + " #2# " + JSON.stringify(options));
    const primaryKeys = [];
    const foreignKeys = {};
    const attrStr = [];
    const selfReferences = {};
    const selfReferencesAttrStr = [];

    for (const attr in attributes) {
      if (!Object.prototype.hasOwnProperty.call(attributes, attr)) continue;
      const dataType = attributes[attr];
      let match;

      if (dataType.includes("PRIMARY KEY")) {
        primaryKeys.push(attr);

        if (dataType.includes("REFERENCES")) {
          // LxDB doesn't support inline REFERENCES declarations: move to the end
          match = dataType.match(/^(.+) (REFERENCES.*)$/);
          attrStr.push(
            `${this.quoteIdentifier(attr)} ${match[1].replace(
              "PRIMARY KEY",
              ""
            )}`
          );
          foreignKeys[attr] = match[2];
        } else {
          attrStr.push(
            `${this.quoteIdentifier(attr)} ${dataType.replace(
              "PRIMARY KEY",
              ""
            )}`
          );
        }
      } else if (dataType.includes("REFERENCES")) {
        // LxDB doesn't support inline REFERENCES declarations: move to the end
        match = dataType.match(/^(.+) (REFERENCES.*)$/);
        if (match[2].startsWith(`REFERENCES "${tableName}"`)) {
          selfReferences[attr] = match[2];
        } else {
          // attrStr.push(`${this.quoteIdentifier(attr)} ${match[1]}`);
          foreignKeys[attr] = match[2];
        }
        attrStr.push(`${this.quoteIdentifier(attr)} ${match[1]}`);
        // foreignKeys[attr] = match[2];
      } else {
        attrStr.push(`${this.quoteIdentifier(attr)} ${dataType}`);
      }
    }

    const table = this.quoteTable(tableName);
    let attributesClause = attrStr.join(", ");
    let selfReferencesAttributesClause = attrStr.join(", ");
    const pkString = primaryKeys
      .map((pk) => this.quoteIdentifier(pk))
      .join(", ");

    //TODO: Review UNIQUE
    if (options.uniqueKeys) {
      _.each(options.uniqueKeys, (columns, indexName) => {
        if (columns.customIndex) {
          if (typeof indexName !== "string") {
            indexName = `uniq_${tableName}_${columns.fields.join("_")}`;
          }
          //attributesClause += `, UNIQUE ${this.quoteIdentifier(indexName)} (${columns.fields.map(field => this.quoteIdentifier(field)).join(', ')})`;
          attributesClause += `, UNIQUE (${columns.fields
            .map((field) => this.quoteIdentifier(field))
            .join(", ")})`;
        }
      });
    }
    if (pkString.length > 0) {
      attributesClause += `, PRIMARY KEY (${pkString})`;
    } else {
      let tpieces = table.replace(/"/g, "").split("_");
      if (
        tpieces.length === 2 &&
        tpieces[0].length > 2 &&
        tpieces[1].length > 2
      ) {
        let t0 =
          tpieces[0][0].toUpperCase() +
          tpieces[0].substr(1, tpieces[0].length - 2);
        let t1 =
          tpieces[1][0].toUpperCase() +
          tpieces[1].substr(1, tpieces[1].length - 2);
        let t0a = t0 + "Id";
        let t1a = t1 + "Id";
        if (attributes[t0a] && attributes[t1a]) {
          attributesClause += `, PRIMARY KEY ("${t1a}","${t0a}")`;
        }
      }
    }

    let constraints = "";
    for (const fkey in foreignKeys) {
      if (Object.prototype.hasOwnProperty.call(foreignKeys, fkey)) {
        attributesClause += `, FOREIGN KEY (${this.quoteIdentifier(fkey)}) ${
          foreignKeys[fkey]
        }`;
      }
      // constraints += `ENABLE TRIGGER "FK_db-USER1-TASKXYZS_UserXYZId" ON "TASKXYZS";`
    }
    for (const fkey in selfReferences) {
      if (Object.prototype.hasOwnProperty.call(selfReferences, fkey)) {
        constraints += `ALTER TABLE ${table} ADD FOREIGN KEY (${this.quoteIdentifier(
          fkey
        )}) ${selfReferences[fkey]};`;
      }
      // constraints += `ENABLE TRIGGER "FK_db-USER1-TASKXYZS_UserXYZId" ON "TASKXYZS";`
    }

    return Utils.joinSQLFragments([
      "CREATE TABLE IF NOT EXISTS",
      table,
      "KEEP MVCC",
      `(${attributesClause})`,
      ";",
      constraints,
    ]);
  }

  describeTableQuery(tableName, schema, schemaDelimiter) {
    return `SELECT * from sysmeta.columns WHERE TABLENAME = '${tableName}' AND TABLESCHEM = '${tschema}'`;
  }

  showTablesQuery(database) {
    let query = "SELECT TABLENAME FROM SYSMETA.TABLES WHERE TABLETYPE='TABLE'";
    query += ` AND TABLESCHEM NOT IN ('SYSMETA', 'LXSYSMETA')`; // OR ${this.escape(database)}`;
    return `${query};`;
  }

  tableExistsQuery(table) {
    return `SELECT TABLENAME FROM SYSMETA.TABLES WHERE TABLETYPE='TABLE' AND TABLESCHEM NOT IN ('SYSMETA', 'LXSYSMETA') AND TABLENAME = '${table}';`;
  }

  addColumnQuery(table, key, dataType) {
    return Utils.joinSQLFragments([
      "ALTER TABLE",
      this.quoteTable(table),
      "ADD",
      this.quoteIdentifier(key),
      this.attributeToSQL(dataType, {
        context: "addColumn",
        tableName: table,
        foreignKey: key,
      }),
      ";",
    ]);
  }

  removeColumnQuery(tableName, attributeName) {
    return Utils.joinSQLFragments([
      "ALTER TABLE",
      this.quoteTable(tableName),
      "DROP",
      this.quoteIdentifier(attributeName),
      ";",
    ]);
  }

  changeColumnQuery(tableName, attributes) {
    const attrString = [];
    const constraintString = [];

    for (const attributeName in attributes) {
      let definition = attributes[attributeName];
      if (definition.includes("REFERENCES")) {
        const attrName = this.quoteIdentifier(attributeName);
        definition = definition.replace(/.+?(?=REFERENCES)/, "");
        constraintString.push(`FOREIGN KEY (${attrName}) ${definition}`);
      } else {
        attrString.push(
          `\"${attributeName}\" \"${attributeName}\" ${definition}`
        );
      }
    }

    return Utils.joinSQLFragments([
      "ALTER TABLE",
      this.quoteTable(tableName),
      attrString.length && `CHANGE ${attrString.join(", ")}`,
      constraintString.length && `ADD ${constraintString.join(", ")}`,
      ";",
    ]);
  }

  renameColumnQuery(tableName, attrBefore, attributes) {
    const attrString = [];

    for (const attrName in attributes) {
      const definition = attributes[attrName];
      attrString.push(`\"${attrBefore}\" \"${attrName}\" ${definition}`);
    }

    return Utils.joinSQLFragments([
      "ALTER TABLE",
      this.quoteTable(tableName),
      "CHANGE",
      attrString.join(", "),
      ";",
    ]);
  }

  handleSequelizeMethod(smth, tableName, factory, options, prepend) {
    if (smth instanceof Utils.Json) {
      if (smth.conditions) {
        const conditions = this.parseConditionObject(smth.conditions).map(
          (condition) =>
            `${this.jsonPathExtractionQuery(
              condition.path[0],
              _.tail(condition.path)
            )} = '${condition.value}'`
        );
        return conditions.join(" AND ");
      }
      if (smth.path) {
        let str;
        if (this._checkValidJsonStatement(smth.path)) {
          str = smth.path;
        } else {
          const paths = _.toPath(smth.path);
          const column = paths.shift();
          str = this.jsonPathExtractionQuery(column, paths);
        }
        if (smth.value) {
          str += util.format(" = %s", this.escape(smth.value));
        }
        return str;
      }
    } else if (smth instanceof Utils.Cast) {
      if (/timestamp/i.test(smth.type)) {
        smth.type = "timestamp";
      } else if (smth.json && /boolean/i.test(smth.type)) {
        smth.type = "varchar";
      } else if (
        /double/i.test(smth.type) ||
        /float/i.test(smth.type) ||
        /real/i.test(smth.type)
      ) {
        smth.type = "double";
      } else if (/integer/i.test(smth.type)) {
        smth.type = "integer";
      } else if (/boolean/i.test(smth.type)) {
        smth.type = "boolean";
      } else if (/text/i.test(smth.type)) {
        smth.type = "varchar";
      }
    }
    return super.handleSequelizeMethod(
      smth,
      tableName,
      factory,
      options,
      prepend
    );
  }

  _toJSONValue(value) {
    // true/false are stored as strings in mysql
    if (typeof value === "boolean") {
      return value.toString();
    }
    // null is stored as a string in mysql
    if (value === null) {
      return "null";
    }
    return value;
  }

  truncateTableQuery(tableName) {
    return `TRUNCATE TABLE ${this.quoteTable(tableName)}`;
  }

  cascadeWhere(tableName, sourceKey, foreignKey, upperWhere, model, options) {
    return `${this.quoteIdentifier(
      foreignKey
    )} = (SELECT ${this.quoteIdentifier(sourceKey)} FROM ${this.quoteTable(
      tableName
    )} WHERE ${upperWhere})`;
  }

  deleteQuery(tableName, where, options = {}, model) {
    let limit = "";
    let query = `DELETE FROM ${this.quoteTable(tableName)}`;

    if (options.limit) {
      limit = ` LIMIT ${this.escape(options.limit)}`;
    }

    if (typeof where !== "string") {
      where = this.getWhereConditions(where, null, model, options);
    }

    if (where) {
      query += ` WHERE ${where}`;
    }

    return query + limit;
  }

  showIndexesQuery(tableName, options) {
    return `SELECT * FROM LXSYSMETA.INDEXES WHERE TABLENAME = '${tableName.toUpperCase()}'`;
  }

  showConstraintsQuery(table, constraintName) {
    const tableName = table.tableName || table;
    const schemaName = table.schema;

    return Utils.joinSQLFragments([
      "SELECT NULL AS constraintCatalog,",
      "FKNAME as constraintName",
      "FKTABLESCHEM as constraintSchema",
      "'FOREIGN_KEY' as constraintType",
      "FKTABLENAME as tableName",
      "FKTABLESCHEM as tableSchema",
      "from LXSYSMETA.FOREIGN_KEYS",
      `WHERE FKTABLENAME='${tableName.toUpperCase()}'`,
      constraintName && `AND FKNAME = '${constraintName.toUpperCase()}'`,
      schemaName && `AND FKTABLESCHEM = '${schemaName.toUpperCase()}'`,
      ";",
    ]);
  }

  removeIndexQuery(tableName, indexNameOrAttributes) {
    let indexName = indexNameOrAttributes;

    if (typeof indexName !== "string") {
      indexName = Utils.underscore(
        `${tableName}_${indexNameOrAttributes.join("_")}`
      );
    }
    indexName = this.quoteIdentifier(indexName);

    //Fix de "schema"."tablename"
    if (indexName[0] === '"') {
      let ixp = indexName.indexOf(".");
      if (ixp > 0 && indexName[ixp - 1] !== '"') {
        indexName =
          indexName.substring(0, ixp) +
          '"."' +
          indexName.substring(ixp + 1, indexName.length);
      }
    }

    /*    indexName.replace(/\./, function (x, offset, string) {
        return ;
}); */

    return Utils.joinSQLFragments(["DROP INDEX IF EXISTS", indexName]);
  }

  addLimitAndOffset(options) {
    let fragment = "";
    /* eslint-disable */
    if (options.limit != null) {
      fragment += " LIMIT " + this.escape(options.limit);
    }
    if (options.offset != null) {
      fragment += " OFFSET " + this.escape(options.offset);
    }
    /* eslint-enable */

    return fragment;
  }

  attributeToSQL(attribute, options) {
    if (!_.isPlainObject(attribute)) {
      attribute = {
        type: attribute,
      };
    }

    const attributeString = attribute.type.toString({
      escape: this.escape.bind(this),
    });
    let template = attributeString;

    if (attribute.allowNull === false) {
      template += " NOT NULL";
    }

    if (attribute.autoIncrement) {
      template += " GENERATED ALWAYS AS IDENTITY START WITH 1 INCREMENT BY 1";
    }

    // BLOB/TEXT/GEOMETRY/JSON cannot have a default value
    if (
      !typeWithoutDefault.has(attributeString) &&
      attribute.type._binary !== true &&
      Utils.defaultValueSchemable(attribute.defaultValue)
    ) {
      template += ` DEFAULT ${this.escape(attribute.defaultValue)}`;
    }

    //TODO: Review UNIQUE
    // if (attribute.unique === true) {
    //   template += ' UNIQUE';
    // }

    if (attribute.primaryKey) {
      template += " PRIMARY KEY";
    }

    /* Don't support COMMENT right now:
    if (attribute.comment) {
      template += ` COMMENT '{this.escape(attribute.comment)}'`;
    }*/

    if (attribute.first) {
      template += " FIRST";
    }
    if (attribute.after) {
      template += ` AFTER ${this.quoteIdentifier(attribute.after)}`;
    }

    if (attribute.references) {
      if (options && options.context === "addColumn" && options.foreignKey) {
        const attrName = this.quoteIdentifier(options.foreignKey);
        const fkName = this.quoteIdentifier(
          `${options.tableName}_${attrName}_foreign_idx`
        );

        template += `, ADD CONSTRAINT ${fkName} FOREIGN KEY (${attrName})`;
      }

      template += ` REFERENCES ${this.quoteTable(attribute.references.model)}`;

      if (attribute.references.key) {
        template += ` (${this.quoteIdentifier(attribute.references.key)})`;
      } else {
        template += ` (${this.quoteIdentifier("id")})`;
      }
      /* Not supported now:
      if (attribute.onDelete) {
        template += ` ON DELETE ${attribute.onDelete.toUpperCase()}`;
      }

      if (attribute.onUpdate) {
        template += ` ON UPDATE ${attribute.onUpdate.toUpperCase()}`;
      }*/
    }

    return template;
  }

  attributesToSQL(attributes, options) {
    const result = {};

    for (const key in attributes) {
      const attribute = attributes[key];
      result[attribute.field || key] = this.attributeToSQL(attribute, options);
    }

    return result;
  }

  /**
   * Check whether the statmement is json function or simple path
   *
   * @param   {string}  stmt  The statement to validate
   * @returns {boolean}       true if the given statement is json function
   * @throws  {Error}         throw if the statement looks like json function but has invalid token
   * @private
   */
  _checkValidJsonStatement(stmt) {
    if (typeof stmt !== "string") {
      return false;
    }

    let currentIndex = 0;
    let openingBrackets = 0;
    let closingBrackets = 0;
    let hasJsonFunction = false;
    let hasInvalidToken = false;

    while (currentIndex < stmt.length) {
      const string = stmt.substr(currentIndex);
      const functionMatches = JSON_FUNCTION_REGEX.exec(string);
      if (functionMatches) {
        currentIndex += functionMatches[0].indexOf("(");
        hasJsonFunction = true;
        continue;
      }

      const operatorMatches = JSON_OPERATOR_REGEX.exec(string);
      if (operatorMatches) {
        currentIndex += operatorMatches[0].length;
        hasJsonFunction = true;
        continue;
      }

      const tokenMatches = TOKEN_CAPTURE_REGEX.exec(string);
      if (tokenMatches) {
        const capturedToken = tokenMatches[1];
        if (capturedToken === "(") {
          openingBrackets++;
        } else if (capturedToken === ")") {
          closingBrackets++;
        } else if (capturedToken === ";") {
          hasInvalidToken = true;
          break;
        }
        currentIndex += tokenMatches[0].length;
        continue;
      }

      break;
    }

    // Check invalid json statement
    if (
      hasJsonFunction &&
      (hasInvalidToken || openingBrackets !== closingBrackets)
    ) {
      throw new Error(`Invalid json statement: ${stmt}`);
    }

    // return true if the statement has valid json function
    return hasJsonFunction;
  }

  /**
   * Generates an SQL query that returns all foreign keys of a table.
   *
   * @param  {object} table  The table.
   * @param  {string} schemaName The name of the schema.
   * @returns {string}            The generated sql query.
   * @private
   */
  getForeignKeysQuery(table, schemaName) {
    const tableName = table.tableName || table;
    return Utils.joinSQLFragments([
      "SELECT",
      FOREIGN_KEY_FIELDS,
      `FROM LXSYSMETA.FOREIGN_KEYS where FKTABLENAME = '${tableName.toUpperCase()}'`,
      `AND FKTABLESCHEM='${schemaName.toUpperCase()}'`,
      "AND PKTABLENAME IS NOT NULL",
      ";",
    ]);
  }

  /**
   * Generates an SQL query that returns the foreign key constraint of a given column.
   *
   * @param  {object} table  The table.
   * @param  {string} columnName The name of the column.
   * @returns {string}            The generated sql query.
   * @private
   */
  getForeignKeyQuery(table, columnName) {
    const quotedSchemaName = table.schema ? wrapSingleQuote(table.schema) : "";
    const quotedTableName = wrapSingleQuote(table.tableName || table);
    const quotedColumnName = wrapSingleQuote(columnName);

    return Utils.joinSQLFragments([
      "SELECT",
      FOREIGN_KEY_FIELDS,
      "FROM LXSYSMETA.FOREIGN_KEYS",
      [
        `PKTABLENAME = ${quotedTableName.toUpperCase()}`,
        table.schema && `AND PKTABLESCHEM = ${quotedSchemaName.toUpperCase()}`,
        `AND PKCOLUMNNAME = ${quotedColumnName.toUpperCase()}`,
      ],
      ") OR (",
      [
        `FKTABLENAME = ${quotedTableName.toUpperCase()}`,
        table.schema && `AND FKTABLESCHEM = ${quotedSchemaName.toUpperCase()}`,
        `AND FKCOLUMNNAME = ${quotedColumnName.toUpperCase()}`,
        "AND PKTABLENAME IS NOT NULL",
      ],
      ")",
    ]);
  }

  /**
   * Generates an SQL query that removes a foreign key from a table.
   *
   * @param  {string} tableName  The name of the table.
   * @param  {string} foreignKey The name of the foreign key constraint.
   * @returns {string}            The generated sql query.
   * @private
   */
  dropForeignKeyQuery(tableName, foreignKey) {
    return Utils.joinSQLFragments([
      "ALTER TABLE",
      this.quoteTable(tableName),
      "DROP FOREIGN KEY",
      this.quoteIdentifier(foreignKey),
      ";",
    ]);
  }

  setIsolationLevelQuery(value, options) {
    return;
  }

  startTransactionQuery(transaction) {
    return "START TRANSACTION;";
  }

  commitTransactionQuery(transaction) {
    return "COMMIT";
  }

  rollbackTransactionQuery(transaction) {
    return "ROLLBACK";
  }
}

// private methods
function wrapSingleQuote(identifier) {
  return Utils.addTicks(identifier, "'");
}

module.exports = LxDBQueryGenerator;
