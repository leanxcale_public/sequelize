"use strict";
var __defProp = Object.defineProperty;
var __getOwnPropSymbols = Object.getOwnPropertySymbols;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __propIsEnum = Object.prototype.propertyIsEnumerable;
var __defNormalProp = (obj, key, value) =>
  key in obj
    ? __defProp(obj, key, {
        enumerable: true,
        configurable: true,
        writable: true,
        value,
      })
    : (obj[key] = value);
var __spreadValues = (a, b) => {
  for (var prop in b || (b = {}))
    if (__hasOwnProp.call(b, prop)) __defNormalProp(a, prop, b[prop]);
  if (__getOwnPropSymbols)
    for (var prop of __getOwnPropSymbols(b)) {
      if (__propIsEnum.call(b, prop)) __defNormalProp(a, prop, b[prop]);
    }
  return a;
};
var __publicField = (obj, key, value) => {
  __defNormalProp(obj, typeof key !== "symbol" ? key + "" : key, value);
  return value;
};

const AbstractConnectionManager = require("sequelize/lib/dialects/abstract/connection-manager");
const SequelizeErrors = require("sequelize/lib/errors");
const { logger } = require("sequelize/lib/utils/logger");
const debuglxdb = logger.debugContext("connection:lxdb");
const DataTypes = require("sequelize/lib/data-types");
const parserStore = require("sequelize/lib/dialects/parserStore")("mysql");
const { promisify } = require("util");
const fs = require("fs");
const appDir = require("path").resolve("./");

process.env.ODBCINI = `${appDir}/libs/odbc.ini`;
process.env.ODBCSYSINI = `${appDir}/libs`;
process.env.LD_LIBRARY_PATH = `${appDir}/libs`;

const _ConnectionManager = class extends AbstractConnectionManager {
  constructor(dialect, sequelize) {
    sequelize.config.port = sequelize.config.port || 1529;
    super(dialect, sequelize);
    this.lib = this._loadDialectModule("odbc");
    this.refreshTypeParser(DataTypes);
    this.realconnection = void 0;
  }
  _loadDialectModule() {
    return super._loadDialectModule("odbc");
  }
  _refreshTypeParser(dataType) {
    parserStore.refresh(dataType);
  }
  _clearTypeParser() {
    parserStore.clear();
  }
  static _typecast(field, next) {
    if (parserStore.get(field.type)) {
      return parserStore.get(field.type)(field, this.sequelize.options, next);
    }
    return next();
  }
  async connect(config) {
    const DSN = `${config.host}`;
    const connectionConfig = {
      host: config.host,
      port: config.port,
      user: config.username,
      schema: config.username,
      //flags: '-FOUND_ROWS',
      password: config.password,
      database: config.database,
      connectionString:
        "DSN=" +
        DSN +
        ";UID=" +
        config.username +
        ";PWD=" +
        config.password +
        ";NoNoTotal=1024",
      timezone: this.sequelize.options.timezone,
      typeCast: ConnectionManager._typecast.bind(this),
      bigNumberStrings: false,
      supportBigNumbers: true,
      ...config.dialectOptions,
    };
    let odbcConfigFileContent = "";
    let odbcConfigFileName = `${appDir}/libs/odbc.ini`;

    try {
      odbcConfigFileContent = fs.readFileSync(odbcConfigFileName, "utf8");
    } catch (err) {
      fs.writeFileSync(odbcConfigFileName, "");
    }
    if (!odbcConfigFileContent.includes(`[${DSN}]`)) {
      odbcConfigFileContent = `[${DSN}]
Description = odbc for nodejs
Driver = Lxodbc
Trace = INFO
TraceFile = /tmp/sql.log
Host = ${config.host}
Database = ${config.db}
Port = ${config.port}
UserName = ${config.username}
Password = ${config.password}
NoNoTotal = 65536
`;
      fs.appendFileSync(odbcConfigFileName, odbcConfigFileContent);
    }
    config["schema"] = config.username;
    let dotrace = false;
    if (config.UseTZ && this.processTZ == void 0) {
      this.processTZ = config.UseTZ;
      if (config.trace) {
        this.trace = config.trace;
        dotrace = true;
        process.on("uncaughtException", function (err) {
          console.error(err && err.stack ? err.stack : err);
        });
      }
    }
    if (this.processTZ) {
      connectionConfig.connectionString =
        connectionConfig.connectionString + ";TZLocal=" + this.processTZ;
    }
    try {
      const connection = await this.lib.connect(
        connectionConfig.connectionString
      );
      return connection;
    } catch (err) {
      console.log(err);
      throw new SequelizeErrors.HostNotReachableError(err);
    }
  }
  async disconnect(connection) {
    // Don't disconnect connections with CLOSED state
    if (connection._closing) {
      debuglxdb(
        "connection tried to disconnect but was already at CLOSED state"
      );
      return;
    }

    //return await promisify(callback => connection.odbcconnection.close(callback))();
    return await promisify((callback) => connection.close(callback))();
  }
  validate(connection) {
    return connection;
  }
};
let ConnectionManager = _ConnectionManager;
__publicField(ConnectionManager, "processTZ");
__publicField(ConnectionManager, "trace");
module.exports = ConnectionManager;
module.exports.ConnectionManager = ConnectionManager;
module.exports.default = ConnectionManager;
//# sourceMappingURL=connection-manager.js.map
