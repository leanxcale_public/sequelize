"use strict";

const wkx = require("wkx");
const _ = require("lodash");
const moment = require("moment-timezone");
const momentWithoutTz = require("moment");
const sequelizeErrors = require("sequelize/lib/errors");
module.exports = (BaseTypes) => {
  BaseTypes.ABSTRACT.prototype.dialectTypes =
    "https://docs.leanxcale.com/leanxcale/1.6/apis/index.html";

  /**
   * types: [buffer_type, ...]
   *
   * @see buffer_type here https://dev.mysql.com/doc/refman/5.7/en/c-api-prepared-statement-type-codes.html
   * @see hex here https://github.com/sidorares/node-mysql2/blob/master/lib/constants/types.js
   */

  BaseTypes.DATE.types.mysql = ["DATETIME"];
  BaseTypes.STRING.types.mysql = ["VARCHAR"];
  BaseTypes.CHAR.types.mysql = ["VARCHAR"];
  BaseTypes.TEXT.types.mysql = ["BLOB"];
  BaseTypes.TINYINT.types.mysql = ["TINY"];
  BaseTypes.SMALLINT.types.mysql = ["SHORT"];
  BaseTypes.MEDIUMINT.types.mysql = ["INTEGER"];
  BaseTypes.INTEGER.types.mysql = ["INTEGER"];
  BaseTypes.BIGINT.types.mysql = ["BIGINT"];
  BaseTypes.FLOAT.types.mysql = ["FLOAT"];
  BaseTypes.TIME.types.mysql = ["TIME"];
  BaseTypes.DATEONLY.types.mysql = ["DATE"];
  BaseTypes.BOOLEAN.types.mysql = ["BOOLEAN"];
  BaseTypes.BLOB.types.mysql = ["BLOB"];
  BaseTypes.DECIMAL.types.mysql = ["DECIMAL"];
  BaseTypes.UUID.types.mysql = false;
  BaseTypes.ENUM.types.mysql = ["VARCHAR"];
  BaseTypes.REAL.types.mysql = ["DOUBLE"];
  BaseTypes.DOUBLE.types.mysql = ["DOUBLE"];
  BaseTypes.GEOMETRY.types.mysql = ["GEOMETRY"];
  BaseTypes.JSON.types.mysql = ["CLOB"];

  class DATE extends BaseTypes.DATE {
    static refDate = new moment();

    toSql() {
      return this._length ? `TIMESTAMP` : "TIMESTAMP";
    }
    _stringify(date, options) {
      date = this._applyTimezone(date, options); //TODO: Review
      if (options.bindParam) {
        return date.valueOf();
      }
      return date.format("YYYY-MM-DD HH:mm:ss.SSS");
    }
    // _sanitize(value, options) {
    //   // if (value instanceof Int8Array)
    //   //   return String.fromCharCode.apply(null, value);
    //   return new Date(value);
    // }
    parse(value, options) {
      value = value.toString();
      if (value === null) {
        return value;
      }
      if (moment.tz.zone(options.timezone)) {
        value = moment.tz(value, options.timezone).toDate();
      } else {
        value = new Date(`${value} ${options.timezone}`);
      }
      return value;
    }
  }

  class DATEONLY extends BaseTypes.DATEONLY {
    parse(value) {
      return value.string();
    }
  }
  class UUID extends BaseTypes.UUID {
    toSql() {
      return "VARCHAR(36)";
    }
  }

  const SUPPORTED_GEOMETRY_TYPES = ["POINT", "LINESTRING", "POLYGON"];

  class GEOMETRY extends BaseTypes.GEOMETRY {
    constructor(type, srid) {
      super(type, srid);
      if (_.isEmpty(this.type)) {
        this.sqlType = this.key;
        return;
      }
      if (SUPPORTED_GEOMETRY_TYPES.includes(this.type)) {
        this.sqlType = this.type;
        return;
      }
      throw new Error(
        `Supported geometry types are: ${SUPPORTED_GEOMETRY_TYPES.join(", ")}`
      );
    }
    parse(value) {
      value = value.buffer();
      // Empty buffer, MySQL doesn't support POINT EMPTY
      // check, https://dev.mysql.com/worklog/task/?id=2381
      if (!value || value.length === 0) {
        return null;
      }
      // For some reason, discard the first 4 bytes
      value = value.slice(4);
      return wkx.Geometry.parse(value).toGeoJSON({ shortCrs: true });
    }
    toSql() {
      return this.sqlType;
    }
  }

  class STRING extends BaseTypes.STRING {
    toSql() {
      if (this._binary) {
        return `VARBINARY(${this._length})`;
      }

      return `VARCHAR(${this._length})`;
    }
    _bindParam(value, options) {
      return options.bindParam(
        this.stringify(value.replaceAll(`'`, `''`), options)
      );
    }
  }

  class ENUM extends BaseTypes.ENUM {
    toSql() {
      return "VARCHAR(255)";
    }
  }

  class JSONTYPE extends BaseTypes.JSON {
    toSql(options) {
      return "CLOB";
    }
    _applyTimezone(date, options) {
      if (options.timezone) {
        if (moment.tz.zone(options.timezone)) {
          return moment(date).tz(options.timezone);
        }
        return (date = momentWithoutTz(date).utcOffset(options.timezone));
      }
      return moment(date);
    }
    _bindParam(value, options) {
      return options.bindParam(
        this.stringify(value, options).replaceAll(`'`, `''`)
      );
    }
    _stringify(value, options) {
      if (typeof value === "object")
        Object.keys(value).forEach((key) => {
          if (value[key] instanceof Date) {
            try {
              value[key] = this._applyTimezone(value[key], options);
            } catch (err) {
              console.log(err);
            }
            value[key] = value[key].format("YYYY-MM-DD HH:mm:ss.SSS");
          }
        });
      return options.operation === "where" && typeof value === "string"
        ? value
        : JSON.stringify(value);
    }
    parse(data) {
      return JSON.parse(data);
    }
  }

  class NOW extends BaseTypes.NOW {
    toSql(options) {
      return "CURRENT_DATE";
    }
  }

  class INTEGER extends BaseTypes.INTEGER {
    toSql(options) {
      return "INTEGER";
    }
  }

  class TINYINT extends BaseTypes.TINYINT {
    toSql(options) {
      return "TINYINT";
    }
  }

  class SMALLINT extends BaseTypes.SMALLINT {
    toSql(options) {
      return "SMALLINT";
    }
  }

  class BOOLEAN extends BaseTypes.BOOLEAN {
    toSql(options) {
      return "BOOLEAN";
    }
    _sanitize(value, options) {
      if (typeof value === "string") return value === "1" ? true : false;
      return value;
    }
  }

  class BIGINT extends BaseTypes.BIGINT {
    toSql(options) {
      return "BIGINT";
    }
  }

  class REAL extends BaseTypes.REAL {
    toSql(options) {
      return "REAL";
    }
  }

  class FLOAT extends BaseTypes.FLOAT {
    toSql(options) {
      return "FLOAT";
    }
  }

  class DOUBLE extends BaseTypes.DOUBLE {
    toSql(options) {
      return "DOUBLE";
    }
  }

  class DECIMAL extends BaseTypes.DECIMAL {
    toSql() {
      if (this._precision || this._scale) {
        return `DECIMAL(${[this._precision, this._scale]
          .filter(_.identity)
          .join(",")})`;
      }
      return "DECIMAL";
    }
  }

  class NUMERIC extends BaseTypes.NUMERIC {
    toSql(options) {
      return "DECIMAL";
    }
  }

  class BLOB extends BaseTypes.BLOB {
    _stringify(blob, options) {
      return blob.toString();
    }

    _bindParam(value, options) {
      if (!Buffer.isBuffer(value)) {
        if (Array.isArray(value)) {
          value = Buffer.from(value);
        } else {
          value = Buffer.from(value.toString());
        }
      }
      return options.bindParam(this._stringify(value));
    }
    _sanitize(value, options) {
      if (value instanceof Int8Array)
        return String.fromCharCode.apply(null, value);
      return value;
    }
    toSql() {
      switch (this._length.toLowerCase()) {
        case "tiny":
          return "VARCHAR";
        case "medium":
          return "BLOB";
        case "long":
          return "BLOB";
        default:
          return this.key;
      }
    }
  }

  /**
   * CHAR A fixed length string
   */
  class CHAR extends BaseTypes.CHAR {
    /**
     * @param {number} [length=255] length of string
     * @param {boolean} [binary=false] Is this binary?
     */
    constructor(length, binary) {
      super((typeof length === "object" && length) || { length, binary });
    }
    toSql() {
      if (this._binary) {
        return "VARCHAR";
      } else {
        return `CHAR(${this._length})`;
      }
    }
  }

  /**
   * Unlimited length TEXT column
   */
  class TEXT extends BaseTypes.TEXT {
    /**
     * @param {string} [length=''] could be tiny, medium, long.
     */
    constructor(length) {
      super();
      const options = (typeof length === "object" && length) || { length };
      this.options = options;
      this._length = options.length || "";
    }
    toSql() {
      switch (this._length.toLowerCase()) {
        case "tiny":
          return "VARCHAR";
        case "medium":
          return "VARCHAR";
        case "long":
          return "VARCHAR";
        default:
          return "VARCHAR";
      }
    }
    validate(value) {
      if (typeof value !== "string") {
        throw new sequelizeErrors.ValidationError(
          util.format("%j is not a valid string", value)
        );
      }
      return true;
    }
  }

  return {
    ENUM,
    DATE,
    DATEONLY,
    UUID,
    DECIMAL,
    JSON: JSONTYPE,
    STRING,
    CHAR,
    TEXT,
    NOW,
    INTEGER,
    TINYINT,
    BOOLEAN,
    SMALLINT,
    BIGINT,
    REAL,
    "DOUBLE PRECISION": DOUBLE,
    NUMERIC,
    FLOAT,
    BLOB,
  };
};
