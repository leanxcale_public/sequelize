"use strict";
const _ = require("lodash");
const {
  QueryInterface,
} = require("sequelize/lib/dialects/abstract/query-interface");
const QueryTypes = require("sequelize/lib/query-types");
const Utils = require("sequelize/lib/utils");
const { logger } = require("sequelize/lib/utils/logger");

const debuglxdb = logger.debugContext("lxdb");
var __defProp = Object.defineProperty;
var __defProps = Object.defineProperties;
var __getOwnPropDescs = Object.getOwnPropertyDescriptors;
var __getOwnPropSymbols = Object.getOwnPropertySymbols;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __propIsEnum = Object.prototype.propertyIsEnumerable;
var __defNormalProp = (obj, key, value) =>
  key in obj
    ? __defProp(obj, key, {
        enumerable: true,
        configurable: true,
        writable: true,
        value,
      })
    : (obj[key] = value);
var __spreadProps = (a, b) => __defProps(a, __getOwnPropDescs(b));
var __spreadValues = (a, b) => {
  for (var prop in b || (b = {}))
    if (__hasOwnProp.call(b, prop)) __defNormalProp(a, prop, b[prop]);
  if (__getOwnPropSymbols)
    for (var prop of __getOwnPropSymbols(b)) {
      if (__propIsEnum.call(b, prop)) __defNormalProp(a, prop, b[prop]);
    }
  return a;
};
/**
 * The interface that Sequelize uses to talk with MySQL/MariaDB database
 */
class LxDBQueryInterface extends QueryInterface {
  /**
   * A wrapper that fixes MySQL's inability to cleanly remove columns from existing tables if they have a foreign key constraint.
   *
   * @override
   */
  async removeColumn(tableName, columnName, options) {
    options = options || {};

    const [results] = await this.sequelize.query(
      this.queryGenerator.getForeignKeyQuery(
        tableName.tableName
          ? tableName
          : {
              tableName,
              schema: this.sequelize.config.database,
            },
        columnName
      ),
      { raw: true, ...options }
    );

    //Exclude primary key constraint
    if (results.length && results[0].constraint_name !== "PRIMARY") {
      await Promise.all(
        results.map((constraint) =>
          this.sequelize.query(
            this.queryGenerator.dropForeignKeyQuery(
              tableName,
              constraint.constraint_name
            ),
            { raw: true, ...options }
          )
        )
      );
    }

    return await this.sequelize.query(
      this.queryGenerator.removeColumnQuery(tableName, columnName),
      { raw: true, ...options }
    );
  }

  /**
   * @override
   */
  async upsert(tableName, insertValues, updateValues, where, options) {
    options = { ...options };

    options.type = QueryTypes.UPSERT;
    // options.updateOnDuplicate = Object.keys(updateValues);
    // options.upsertKeys = Object.values(options.model.primaryKeys).map(
    //   (item) => item.field
    // );

    const model = options.model;
    let sql = this.queryGenerator.insertQuery(
      tableName,
      insertValues,
      model.rawAttributes,
      options
    );
    sql.query = sql.query.replace("INSERT INTO", "UPSERT INTO");
    return await this.sequelize.query(sql, options);
  }

  /**
   * @override
   */
  async removeConstraint(tableName, constraintName, options) {
    //Not implemented yet. Do nothing
  }

  /**
   * @override
   */
  async getForeignKeysForTables(tableNames, options) {
    if (tableNames.length === 0) {
      return {};
    }
    options = { ...options, type: QueryTypes.FOREIGNKEYS };

    const results = await Promise.all(
      tableNames.map((tableName) =>
        this.sequelize.query(
          this.queryGenerator.getForeignKeysQuery(
            tableName,
            this.sequelize.config.schema
              ? this.sequelize.config.schema
              : this.sequelize.config.username
          ),
          options
        )
      )
    );

    const result = {};

    tableNames.forEach((tableName, i) => {
      if (_.isObject(tableName)) {
        tableName = `${tableName.schema}.${tableName.tableName}`;
      }

      result[tableName] = Array.isArray(results[i])
        ? results[i].map((r) => r.constraint_name)
        : [results[i] && results[i].constraint_name];

      result[tableName] = result[tableName].filter(_.identity);
    });

    return result;
  }

  deleteCascadeAssociations(
    hashmapsqls,
    associations,
    upperWhere,
    level,
    options,
    model
  ) {
    level++;
    if (level > 10) {
      throw new Error("Too many nested cascade onDelete for BulkDelete");
    }
    let deletesqls = [];
    const thisobj = this;
    Object.keys(associations).forEach((aname, ixassoc) => {
      const assoc = associations[aname];
      if (
        assoc.options.onDelete == "cascade" &&
        assoc.sourceKeyField &&
        assoc.foreignKeyField
      ) {
        const where = thisobj.queryGenerator.cascadeWhere(
          assoc.source.tableName,
          assoc.sourceKeyField,
          assoc.foreignKeyField,
          upperWhere,
          model,
          options
        );
        thisobj.deleteCascadeAssociations(
          hashmapsqls,
          assoc.target.associations,
          where,
          level,
          options,
          model
        );
        const sql = thisobj.queryGenerator.deleteQuery(
          assoc.target.tableName,
          where,
          options,
          model
        );
        if (hashmapsqls[sql] === true) {
          //Detected
        } else {
          debuglxdb("CASCADE: " + sql);
          deletesqls.push(sql);
          hashmapsqls[sql] = true;
        }
        debuglxdb("HERE:" + level + ":" + sql + ":" + deletesqls);
      }
    });
    //Guarantee order strictly:
    let sqlpromises = [];
    deletesqls.forEach((sql) => {
      sqlpromises.push(thisobj.sequelize.query(sql, options));
      debuglxdb("deleteCascade: " + level + ":" + sql);
    });
    if (sqlpromises.length > 0) {
      const p = Promise.all(sqlpromises);
      p.then((r) => {
        debuglxdb("deleteCascadeAssociations.level DONE");
      }).catch((err) => {
        debuglxdb("deleteCascadeAssociations.level ERROR");
        throw new Error(err);
      });
      return p;
    }
    return Promise.resolve([]);
  }
  /* There is another syntax that may be worth trying:
  WITH T1 (SELECT sourceKeyField FROM source.tableName WHERE),
    T2 (SELECT ... FROM l2.target.tableNAme, T1 WHERE l2.foreignKeyField = source) ... */

  /**
   * Delete multiple records from a table
   *
   * @param {string}  tableName            table name from where to delete records
   * @param {object}  where                where conditions to find records to delete
   * @param {object}  [options]            options
   * @param {boolean} [options.truncate]   Use truncate table command
   * @param {boolean} [options.cascade=false]         Only used in conjunction with TRUNCATE. Truncates  all tables that have foreign-key references to the named table, or to any tables added to the group due to CASCADE.
   * @param {boolean} [options.restartIdentity=false] Only used in conjunction with TRUNCATE. Automatically restart sequences owned by columns of the truncated table.
   * @param {Model}   [model]              Model
   *
   * @returns {Promise}
   */
  async getForeignKeyReferencesForTable(tableName, options) {
    const queryOptions = __spreadProps(__spreadValues({}, options), {
      type: QueryTypes.FOREIGNKEYS,
    });
    const query = this.queryGenerator.getForeignKeysQuery(
      tableName,
      this.sequelize.config.username
    );
    return this.sequelize.query(query, queryOptions);
  }
  async bulkDelete(tableName, where, options, model) {
    options = Utils.cloneDeep(options);
    options = _.defaults(options, { limit: null });

    if (options.truncate === true) {
      return this.sequelize.query(
        this.queryGenerator.truncateTableQuery(tableName, options),
        options
      );
    }

    if (typeof identifier === "object") where = Utils.cloneDeep(where);

    let hashmapsqls = {}; //To avoid repetition of DELETE

    if (
      model &&
      model.associations &&
      Object.keys(model.associations).length > 0
    ) {
      const wheretxt = this.queryGenerator.getWhereConditions(
        where,
        null,
        model,
        options
      );
      let level = 0;
      await this.deleteCascadeAssociations(
        hashmapsqls,
        model.associations,
        wheretxt,
        level,
        options,
        model
      );
    }
    return await this.sequelize.query(
      this.queryGenerator.deleteQuery(tableName, where, options, model),
      options
    );
  }
}

exports.LxDBQueryInterface = LxDBQueryInterface;
