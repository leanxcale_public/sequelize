"use strict";

const _ = require("lodash");
const AbstractDialect = require("sequelize/lib/dialects/abstract/index");
const ConnectionManager = require("./connection-manager");
const Query = require("./query");
const QueryGenerator = require("./query-generator");
const DataTypes = require("../DataTypes").mysql;
const { LxDBQueryInterface } = require("./query-interface");

class LxDBDialect extends AbstractDialect {
  constructor(sequelize) {
    super();
    this.sequelize = sequelize;
    this.connectionManager = new ConnectionManager(this, sequelize);
    this.queryGenerator = new QueryGenerator({
      _dialect: this,
      sequelize,
    });
    this.queryInterface = new LxDBQueryInterface(
      sequelize,
      this.queryGenerator
    );
  }
}

LxDBDialect.prototype.supports = _.merge(
  _.cloneDeep(AbstractDialect.prototype.supports),
  {
    "VALUES ()": true,
    "LIMIT ON UPDATE": true,
    bulkDefault: true,
    lock: true,
    transactions: true,
    forShare: "LOCK IN SHARE MODE",
    settingIsolationLevelDuringTransaction: false,
    schemas: true,
    inserts: {
      ignoreDuplicates: " IGNORE",
      updateOnDuplicate: " ON DUPLICATE KEY UPDATE",
    },
    index: {
      collate: false,
      length: true,
      parser: true,
      type: true,
      using: 1,
    },
    constraints: {
      dropConstraint: false,
      check: false,
      unique: false,
    },
    autoIncrement: {
      identityInsert: false,
      defaultValue: true,
      update: false,
    },
    indexHints: true,
    NUMERIC: true,
    GEOMETRY: false,
    JSON: true,
    REGEXP: true,
    returnValues: true,
  }
);

LxDBDialect.prototype.defaultVersion = "1.7.0";
LxDBDialect.prototype.Query = Query;
LxDBDialect.prototype.QueryGenerator = QueryGenerator;
LxDBDialect.prototype.DataTypes = DataTypes;
LxDBDialect.prototype.name = "lxdb";
LxDBDialect.prototype.TICK_CHAR = '"';
LxDBDialect.prototype.TICK_CHAR_LEFT = LxDBDialect.prototype.TICK_CHAR;
LxDBDialect.prototype.TICK_CHAR_RIGHT = LxDBDialect.prototype.TICK_CHAR;

module.exports = LxDBDialect;
