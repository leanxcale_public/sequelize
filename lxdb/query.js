"use strict";
var __defProp = Object.defineProperty;
var __getOwnPropSymbols = Object.getOwnPropertySymbols;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __propIsEnum = Object.prototype.propertyIsEnumerable;
var __defNormalProp = (obj, key, value) =>
  key in obj
    ? __defProp(obj, key, {
        enumerable: true,
        configurable: true,
        writable: true,
        value,
      })
    : (obj[key] = value);
var __spreadValues = (a, b) => {
  for (var prop in b || (b = {}))
    if (__hasOwnProp.call(b, prop)) __defNormalProp(a, prop, b[prop]);
  if (__getOwnPropSymbols)
    for (var prop of __getOwnPropSymbols(b)) {
      if (__propIsEnum.call(b, prop)) __defNormalProp(a, prop, b[prop]);
    }
  return a;
};
const asyncjs = require("async");
const AbstractQuery = require("sequelize/lib/dialects/abstract/query");
const sequelizeErrors = require("sequelize/lib/errors");
const _ = require("lodash");
const { logger } = require("sequelize/lib/utils/logger");
const DataTypes = require("../DataTypes");
const momentTz = require("moment-timezone");
const { resolve } = require("path/posix");
const ER_DUP_ENTRY = 1062;
const ER_ROW_IS_REFERENCED = 1451;
const ER_NO_REFERENCED_ROW = 1452;
const debuglxdb = logger.debugContext("sql:lxdb");
const { Mutex } = require("async-mutex");

const formatBindParametersAbstract = (
  sql,
  values,
  dialect,
  replacementFunc,
  options
) => {
  if (!values) {
    return [sql, []];
  }

  options = options || {};
  if (typeof replacementFunc !== "function") {
    options = replacementFunc || {};
    replacementFunc = undefined;
  }

  if (!replacementFunc) {
    if (options.skipValueReplace) {
      replacementFunc = (match, key, values) => {
        if (values[key] !== undefined) {
          return match;
        }
        return undefined;
      };
    } else {
      replacementFunc = (match, key, values, timeZone, dialect) => {
        if (values[key] !== undefined) {
          return SqlString.escape(values[key], timeZone, dialect);
        }
        return undefined;
      };
    }
  } else if (options.skipValueReplace) {
    const origReplacementFunc = replacementFunc;
    replacementFunc = (match, key, values, timeZone, dialect, options) => {
      if (
        origReplacementFunc(match, key, values, timeZone, dialect, options) !==
        undefined
      ) {
        return match;
      }
      return undefined;
    };
  }

  const timeZone = null;
  const list = Array.isArray(values);
  sql = sql.replace(/\B\$(\$|\w+)/g, (match, key) => {
    if ("$" === key) {
      return options.skipUnescape ? match : key;
    }

    let replVal;
    if (list) {
      if (key.match(/^[1-9]\d*$/)) {
        key = key - 1;
        replVal = replacementFunc(
          match,
          key,
          values,
          timeZone,
          dialect,
          options
        );
      }
    } else if (!key.match(/^\d*$/)) {
      replVal = replacementFunc(match, key, values, timeZone, dialect, options);
    }
    if (replVal === undefined) {
      throw new Error(
        `Named bind parameter "${match}" has no value in the given object.`
      );
    }
    return replVal;
  });
  return [sql, []];
};
function toJson(data) {
  if (data !== void 0) {
    return JSON.stringify(data, (_2, v) =>
      typeof v === "bigint" ? `${v}#bigint` : v
    ).replace(/"(-?\d+)#bigint"/g, (_2, a) => a);
  }
}
class Query extends AbstractQuery {
  querylock = new Mutex();
  constructor(connection, sequelize, options) {
    super(
      connection,
      sequelize,
      __spreadValues({ showWarnings: false }, options)
    );
  }
  static formatBindParameters(sql, values, dialect) {
    const bindParam = [];
    const replacementFunc = (match, key, values_) => {
      if (values_[key] !== void 0) {
        bindParam.push(values_[key]);
        return "?";
      }
      return void 0;
    };
    sql = formatBindParametersAbstract(
      sql,
      values,
      dialect,
      replacementFunc
    )[0];
    return [sql, bindParam.length > 0 ? bindParam : void 0];
  }
  async run(sql, parameters) {
    this.sql = sql;
    const { connection, options, model } = this;
    const showWarnings =
      this.sequelize.options.showWarnings || options.showWarnings;
    if (sql.substr(-1) == ";") {
      sql = sql.substr(0, sql.length - 1);
    }
    const complete = this._logQuery(sql, debuglxdb, parameters);

    if (parameters) {
      debuglxdb("parameters(%j)", parameters);
    }
    let results;
    try {
      let currentTranscation = connection?.connObj;
      // if (sql === "START TRANSACTION") {
      //   console.log(`ROLLBACK;\nexec setAutocommit(false);`);
      //   await new Promise((resolve, reject) => {
      //     currentTranscation.conn.setAutoCommit(
      //       false,
      //       function (err, statement) {
      //         if (err) {
      //           reject(err);
      //         } else {
      //           resolve(statement);
      //         }
      //       }
      //     );
      //   });
      //   return;
      // }
      if (sql === "COMMIT") {
        results = await new Promise((resolve, reject) => {
          return connection.commit((error) =>
            error ? reject(error) : resolve([])
          );
        });
      } else if (sql === "ROLLBACK") {
        results = await new Promise((resolve, reject) => {
          return connection.rollback((error) =>
            error ? reject(error) : resolve([])
          );
        });
      } else if (sql === "START TRANSACTION") {
        results = await new Promise((resolve, reject) => {
          return connection.beginTransaction((error) =>
            error ? reject(error) : resolve([])
          );
        });
      } else {
        let returnState = 0;
        let params;
        if (
          this.isInsertQuery() ||
          this.isUpsertQuery() ||
          this.isUpdateQuery() ||
          this.isDeleteQuery() ||
          this.isBulkDeleteQuery() ||
          this.isBulkUpdateQuery()
        ) {
          if (parameters && parameters.length) {
            let sqlParts = sql.split("?");
            for (let i = 0; i < parameters.length; i++) {
              let param = parameters[i];
              sqlParts[i] += typeof param === "string" ? `'${param}'` : param;
            }
            sql = sqlParts.join("");
          }
          if (sql.includes("RETURNINGSEQ")) {
            params = sql.split(/[()]+/).filter(function (e) {
              return e;
            });
            const attrs = params[1].split(",");
            const values = params[3].split(",");
            params = {};
            for (let i = 0; i < attrs.length; i++) params[attrs[i]] = values[i];
            returnState = 1;
          } else {
            returnState = 2;
          }
        } else {
          returnState = 3;
        }
        let statements = sql.split(";");
        for (let statementSql of statements) {
          results = await new Promise(async (resolve, reject) => {
            const releaselock = await this.querylock.acquire();

            connection.query(statementSql, function (err, results) {
              releaselock();
              if (err) {
                if (err.message.includes("No key in table"))
                  err.code = ER_NO_REFERENCED_ROW;
                reject(err);
              } else {
                if (returnState === 1) {
                  let result = [];
                  if (
                    params[`"${model.autoIncrementAttribute}"`] !== undefined &&
                    params[`"${model.autoIncrementAttribute}"`] != "DEFAULT"
                  )
                    result.push({
                      [model.autoIncrementAttribute]:
                        params[`"${model.autoIncrementAttribute}"`],
                    });
                  else if (results?.[0]?.RESULT !== undefined)
                    result.push({
                      [model.autoIncrementAttribute]: results?.[0]?.RESULT,
                    });
                  resolve(result);
                } else if (returnState === 2) {
                  resolve({ affectedRows: results });
                } else resolve(results);
              }
            });
          });
        }
        if (statements.length > 1) results = [true];
      }
    } catch (err) {
      // console.log(sql);
      // console.log("the error is, ", err);
      if (options.transaction && err.errno === 1213) {
        options.transaction.finished = "rollback";
      }
      err.sql = sql;
      err.parameters = parameters;
      throw this.formatError(err);
    }

    // console.log(sql);

    if (complete) {
      complete();
    }
    debuglxdb("lxdb:result:" + JSON.stringify(toJson(results)));
    if (showWarnings && results && results.warningStatus > 0) {
      await this.logWarnings(results);
    }
    return this.formatResults(results);
  }

  isDeleteQuery() {
    return this.options.type === "DELETE";
  }

  handleInsertQuery(results, metaData, InsertIdField) {
    if (this.instance) {
      const autoIncrementAttribute = this.model.autoIncrementAttribute;
      let id = null;
      id = id || (results && results[this.InsertIdField]);
      id = id || (metaData && metaData[this.InsertIdField]);
      this.instance[autoIncrementAttribute] = id;
    }
  }
  formatResults(data) {
    let result = this.instance;
    if (this.isInsertQuery(data) || this.isUpsertQuery(data)) {
      if (0 in data) {
        let field = void 0;
        if (data[0] instanceof Array && data[0].length > 0) {
          field = Object.keys(data[0][0])[0];
          result = [];
          for (var i = 0; i < data[0].length; i++) {
            result.push(parseInt(data[0][i][field]));
          }
        } else {
          if (
            result &&
            "dataValues" in result &&
            this.model &&
            this.model.autoIncrementAttribute
          ) {
            field = Object.keys(data[0])[0];
            result.dataValues[this.model.autoIncrementAttribute] = parseInt(
              data[0][field]
            );
          }
        }
        this.handleInsertQuery(data, void 0, field);
      } else {
        this.handleInsertQuery(data);
        if (!this.instance) {
          if (
            data.constructor.name === "ResultSetHeader" &&
            this.model &&
            this.model.autoIncrementAttribute &&
            this.model.autoIncrementAttribute ===
              this.model.primaryKeyAttribute &&
            this.model.rawAttributes[this.model.primaryKeyAttribute]
          ) {
            const startId = data[this.getInsertIdField()];
            debuglxdb("startId: " + startId);
            result = [];
            for (let i2 = startId; i2 < startId + data.affectedRows; i2++) {
              result.push({
                [this.model.rawAttributes[this.model.primaryKeyAttribute]
                  .field]: i2,
              });
            }
          } else {
            result = data[this.getInsertIdField()];
            debuglxdb("idResult: " + result);
          }
        }
      }
    }
    if (this.isSelectQuery()) {
      this.handleDateFields(data);
      this.handleJsonSelectQuery(data);
      return this.handleSelectQuery(data);
      // return this.handleSelectQuery(data);
    }
    if (this.isShowTablesQuery()) {
      return this.handleShowTablesQuery(data);
    }
    if (this.isDescribeQuery()) {
      result = {};
      for (const _result of data) {
        const enumRegex = /^enum/i;
        result[_result.Field] = {
          type: enumRegex.test(_result.Type)
            ? _result.Type.replace(enumRegex, "ENUM")
            : _result.Type.toUpperCase(),
          allowNull: _result.Null === "YES",
          defaultValue: _result.Default,
          primaryKey: _result.Key === "PRI",
          autoIncrement:
            Object.prototype.hasOwnProperty.call(_result, "Extra") &&
            _result.Extra.toLowerCase() === "auto_increment",
          comment: _result.Comment ? _result.Comment : null,
        };
      }
      return result;
    }
    if (this.isShowIndexesQuery()) {
      return this.handleShowIndexesQuery(data);
    }
    if (this.isCallQuery()) {
      return data[0];
    }
    if (this.isBulkUpdateQuery() || this.isBulkDeleteQuery()) {
      return data.affectedRows;
    }
    if (this.isVersionQuery()) {
      return data[0].version;
    }
    if (this.isForeignKeysQuery()) {
      return data;
    }
    if (this.isUpsertQuery()) {
      return [result, null];
    }
    if (this.isInsertQuery() || this.isUpdateQuery()) {
      return [result, data.affectedRows];
    }
    if (this.isShowConstraintsQuery()) {
      return data;
    }
    if (this.isRawQuery()) {
      return [data, data];
    }
    return result;
  }
  handleDateFields(rows) {
    if (!this.model || !this.model.fieldRawAttributesMap) {
      return;
    }
    for (const _field of Object.keys(this.model.fieldRawAttributesMap)) {
      const modelField = this.model.fieldRawAttributesMap[_field];
      if (modelField.type instanceof DataTypes.DATE) {
        rows = rows.map((row) => {
          if (row[modelField.fieldName] && modelField.type.parse) {
            row[modelField.fieldName] = modelField.type.parse(
              row[modelField.fieldName],
              this.sequelize.options
            );
          }
          return row;
        });
      }
    }
  }
  handleJsonSelectQuery(rows) {
    if (!this.model || !this.model.fieldRawAttributesMap) {
      return;
    }
    for (const _field of Object.keys(this.model.fieldRawAttributesMap)) {
      const modelField = this.model.fieldRawAttributesMap[_field];
      if (modelField.type instanceof DataTypes.JSON) {
        rows = rows.map((row) => {
          if (row[modelField.fieldName] && modelField.type.parse) {
            row[modelField.fieldName] = modelField.type.parse(
              row[modelField.fieldName]
            );
          }
          return row;
        });
      }
    }
  }
  async logWarnings(results) {
    const warningResults = await this.run("SHOW WARNINGS");
    const warningMessage = `MySQL Warnings (${
      this.connection.uuid || "default"
    }): `;
    const messages = [];
    for (const _warningRow of warningResults) {
      if (
        _warningRow === void 0 ||
        typeof _warningRow[Symbol.iterator] !== "function"
      ) {
        continue;
      }
      for (const _warningResult of _warningRow) {
        if (Object.prototype.hasOwnProperty.call(_warningResult, "Message")) {
          messages.push(_warningResult.Message);
        } else {
          for (const _objectKey of _warningResult.keys()) {
            messages.push([_objectKey, _warningResult[_objectKey]].join(": "));
          }
        }
      }
    }
    this.sequelize.log(warningMessage + messages.join("; "), this.options);
    return results;
  }
  formatError(err) {
    const errCode = err.errno || err.code;
    // console.log("ERRORF: " + JSON.stringify(err));
    // console.log("ERROR2F: " + err.code);
    // console.log("ERROR2F: " + err.name);
    // console.log("ERROR2F: " + err.message);
    // console.log("ERROR2F: " + err.stack);
    switch (errCode) {
      case ER_DUP_ENTRY: {
        const match = err.message.match(
          /Duplicate entry '([\s\S]*)' for key '?((.|\s)*?)'?$/
        );
        let fields = {};
        let message = "Validation error";
        const values = match ? match[1].split("-") : void 0;
        const fieldKey = match ? match[2] : void 0;
        const fieldVal = match ? match[1] : void 0;
        const uniqueKey = this.model && this.model.uniqueKeys[fieldKey];
        if (uniqueKey) {
          if (uniqueKey.msg) message = uniqueKey.msg;
          fields = _.zipObject(uniqueKey.fields, values);
        } else {
          fields[fieldKey] = fieldVal;
        }
        const errors = [];
        _.forOwn(fields, (value, field) => {
          errors.push(
            new sequelizeErrors.ValidationErrorItem(
              this.getUniqueConstraintErrorMessage(field),
              "unique violation",
              field,
              value,
              this.instance,
              "not_unique"
            )
          );
        });
        return new sequelizeErrors.UniqueConstraintError({
          message,
          errors,
          parent: err,
          fields,
        });
      }
      case ER_ROW_IS_REFERENCED:
      case ER_NO_REFERENCED_ROW:
        // index = errMessage.match(/violates foreign key constraint "(.+?)"/);
        // index = index ? index[1] : void 0;
        // table = errMessage.match(/on table "(.+?)"/);
        // table = table ? table[1] : void 0;
        return new sequelizeErrors.ForeignKeyConstraintError({
          message: "Foreign key error",
          fields: null,
          index: null,
          table: null,
          parent: err,
          stack: err.stack,
        });
      default:
        return new sequelizeErrors.DatabaseError(err);
    }
  }
  handleShowIndexesQuery(data) {
    data = data.reduce((acc, item) => {
      if (!(item.Key_name in acc)) {
        acc[item.Key_name] = item;
        item.fields = [];
      }
      acc[item.Key_name].fields[item.Seq_in_index - 1] = {
        attribute: item.Column_name,
        length: item.Sub_part || void 0,
        order: item.Collation === "A" ? "ASC" : void 0,
      };
      delete item.column_name;
      return acc;
    }, {});
    return _.map(data, (item) => ({
      primary: item.Key_name === "PRIMARY",
      fields: item.fields,
      name: item.Key_name,
      tableName: item.Table,
      unique: item.Non_unique !== 1,
      type: item.Index_type,
    }));
  }
}
module.exports = Query;
module.exports.Query = Query;
module.exports.default = Query;
//# sourceMappingURL=query.jsmap
